const menuToggle = document.querySelector(".menu__dropdown-btn");
const menuDropdown = document.querySelector (".menu__dropdown");
const menuNavigation = document.querySelector (".menu__navigation");
menuToggle.onclick = function () {
    menuDropdown.classList.toggle('active');
    menuToggle.classList.toggle('active');
    menuNavigation.classList.toggle('active');
}